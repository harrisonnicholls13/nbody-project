#include "nbody.h"

/**
 * Get random double in range min to max
 * @param min       Minimum of bound of range
 * @param max       Maximum of bound of range
 */
double frand( double min, double max ) {
    double scale = rand() / (double) RAND_MAX; 
    return min + scale * ( max - min );
}

/**
 * Allocate memory for a 2D array
 * @param n1        Length of first dimension
 * @param n2        Length of second dimension
 * @return          Pointer to array
 */
double** malloc2D(int n1, int n2) {
    double** v1 = (double**) malloc(n1*sizeof(double*));
    if (v1 == NULL) {
        printf(PRT_ERROR"Could not allocate memory \n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < n1; i++) {
        v1[i] = (double*) malloc(n2*sizeof(double));
        if (v1[i] == NULL) {
            printf(PRT_ERROR"Could not allocate memory \n");
            exit(EXIT_FAILURE);
        }
    }
    return v1;
}

/**
 * Free memory allocated to a 2D array
 * @param v         Pointer to first element of array
 * @param n1        Length of first dimension
 */
void free2D(double** v, int n1) {
    for (int i = 0; i < n1; i++) {
        free(v[i]);
    }
    free(v);
}


/**
 * Copy data between two 1D arrays (double)
 * @param src       Pointer to source
 * @param dst       Pointer to destination
 * @param n1        Length of first dimension
 */
void copy1D(double* src, double* dst, int n1) {
    for (int i = 0; i < n1; i++) {
        dst[i] = src[i];
    }
}

/**
 * Copy data between two 2D arrays (double)
 * @param src       Pointer to source
 * @param dst       Pointer to destination
 * @param n1        Length of first dimension
 * @param n1        Length of second dimension
 */
void copy2D(double** src, double** dst, int n1, int n2) {
    for (int i = 0; i < n1; i++) {
        for (int j = 0; j < n2; j++) {
            dst[i][j] = src[i][j];
        }
    }
}

/**
 * Allocate dummy-memory to variables inside state structure
 * @param src       Pointer to state structure
 * @param temp_N    Number of particles to initialise with
 */
void state_init(State_t* s, int temp_N) {
    s->n =      temp_N;
    s->v_max =  0.0;
    s->alive =  (bool*) malloc(temp_N*sizeof(bool));
    s->rho =    (double*) malloc(temp_N*sizeof(double));
    s->Rp =     (double*) malloc(temp_N*sizeof(double));
    s->m =      (double*) malloc(temp_N*sizeof(double));
    s->ps =     (char**)  malloc(STRLEN * temp_N * sizeof(char));
    s->si =     malloc2D(temp_N,DIMS);
    s->so =     malloc2D(temp_N,DIMS);
    s->vh =     malloc2D(temp_N,DIMS);
    s->vi =     malloc2D(temp_N,DIMS);
    s->vo =     malloc2D(temp_N,DIMS);
    s->ai =     malloc2D(temp_N,DIMS);
    s->eT =     0.0;
    s->eV =     0.0;
    s->eS =     0.0;
    s->Lx =     0.0;
    s->Ly =     0.0;
    s->Lz =     0.0;
    s->com_so = (double*) calloc(DIMS,sizeof(double));
    s->com_si = (double*) calloc(DIMS,sizeof(double));
    s->com_vi = (double*) calloc(DIMS,sizeof(double));
}

/**
 * Copy data between two state structures
 * @param src       Pointer to source
 * @param dst       Pointer to destination
 */
void state_cpy(State_t* src, State_t* dst) {
    dst->n = src->n;
    dst->v_max = src->v_max;
    for (int s = 0; s < src->n; s++) {
        strcpy(src->ps[s],dst->ps[s]);
    }
    copy1D(src->rho, dst->rho, src->n);
    copy1D(src->Rp,  dst->Rp,  src->n);
    copy1D(src->m,   dst->m,   src->n);
    copy1D(src->com_si,dst->com_si,DIMS);
    copy1D(src->com_so,dst->com_so,DIMS);
    copy1D(src->com_vi,dst->com_vi,DIMS);
    copy2D(src->so,dst->so,src->n,DIMS);
    copy2D(src->si,dst->si,src->n,DIMS);
    copy2D(src->vi,dst->vi,src->n,DIMS);
    copy2D(src->vo,dst->vo,src->n,DIMS);
    copy2D(src->vh,dst->vh,src->n,DIMS);
    copy2D(src->ai,dst->ai,src->n,DIMS);
}

/**
 * Free memory allocated to a state structure
 * @param state     Pointer to state structure
 */
void state_free(State_t* state) {
    free(state->ps);
    free2D(state->si,state->n);
    free2D(state->so,state->n);
    free2D(state->vi,state->n);
    free2D(state->vh,state->n);
    free2D(state->ai,state->n);
    free2D(state->vo,state->n);
    free(state->rho);
    free(state->Rp);
    free(state->m);
    free(state);
    free(state->com_si);
    free(state->com_so);
    free(state->com_vi);
}
