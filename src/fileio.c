#include "nbody.h"

/**
 * Read initial state of the system from a file
 * @param state     System stucture
 * @param path      Path to file
 * @param print     Print information about the system to stdout?
 */
void read_init(State_t* state, char* path, bool print) {

    if (print)
        printf(PRT_MSG"Reading system file from '%s' \n",path);

    FILE* file = fopen(path, "r");

    if (file == NULL) {
        printf(PRT_ERROR"Could not open init file \n");
        exit(EXIT_FAILURE);
    }

    char    line_name[STRLEN];
    char    line_alive[STRLEN];
    double  line_sr = 0.0; // pos (radial)
    double  line_st = 0.0; // pos (phi)
    double  line_sp = 0.0; // pos (theta)
    double  line_sx = 0.0; // pos (x)
    double  line_sy = 0.0; // pos (y)
    double  line_sz = 0.0; // pos (z)
    double  line_vr = 0.0; // vel (radial)
    double  line_vt = 0.0; // vel (theta)
    double  line_vp = 0.0; // vel (phi)
    double  line_vx = 0.0; // vel (x)
    double  line_vy = 0.0; // vel (y)
    double  line_vz = 0.0; // vel (z)
    double  line_dn = 0.0; // density
    double  line_rp = 0.0; // radius
    int     i = -1;

    // Skip first line in file, which is a comment
    fscanf(file, "%*[^\n]\n");

    // loop over lines in file
    int fsf_ret = 0;
	while ((fsf_ret = fscanf(file, "%s %s %lg %lg %lg %lg %lg %lg %lg %lg \n", 
            line_name, line_alive, &line_dn, &line_rp, &line_sr, &line_sp, &line_st, &line_vr, &line_vp, &line_vt)) != EOF) { 

        if (fsf_ret != 10) {
            printf(PRT_WARN"Line %d of system file is either malformatted or is a comment \n",i+2);
            continue;
        }

        i += 1;

        // if name is 'rand', use random values
        if (STREQ(line_name,"rand")) {
            strcpy(line_alive,"true");
            line_sr = frand(0.5, 6.0);
            line_sp = frand(0, 2.0*pi_c);
            line_st = 0.0;
            line_vr = 0.0;
            line_vt = 0.0;
            line_vp = frand(20, 35);
            line_rp = frand(0.1, 4.9);
            line_dn = frand(0.1, 6.9);
        }

        // Convert from spherical to cartesian (pos and vel)
        // pos
        line_sx  = line_sr * cos(line_sp) * cos(line_st);
        line_sy  = line_sr * sin(line_sp) * cos(line_st);
        line_sz  = line_sr * sin(line_st);
        //vel
        line_vz  = line_vt * cos(line_st);
        line_vr += line_vt * sin(line_st);
        line_vx  = line_vr * cos(pi_c - line_sp)        - line_vp * sin(pi_c - line_sp);
        line_vy  = line_vr * sin(pi_c - line_sp) * -1.0 - line_vp * cos(pi_c - line_sp);
        
        // print to stdout
        if (print) {
            printf(PRT_MSG"Registered body %3d: %-10s ρ = %.2e g.cm-3,  R = %.2e R🜨,  pos = (%+.2e, %+.2e, %+.2e) AU,  vel = (%+.2e, %+.2e, %+.2e) km.s-1\n", 
                i, line_name, line_dn, line_rp, line_sx, line_sy, line_sz, line_vx, line_vy, line_vz);
        }
        
        // make space for new particle and set values
        state->ps       = (char**) realloc(state->ps,(i+1)*sizeof(char*));
        state->ps[i]    = (char*) malloc(STRLEN*sizeof(char));
        strcpy(state->ps[i],line_name);

        state->alive    = (bool*) realloc(state->alive,(i+1)*sizeof(bool));
        state->alive[i] = STREQ(line_alive,"true");

        state->rho      = (double*) realloc(state->rho,(i+1)*sizeof(double));
        state->rho[i]   = fabs(line_dn) * rho_c / (UNIT_M/pow(UNIT_L,3));

        state->Rp       = (double*) realloc(state->Rp,(i+1)*sizeof(double));
        state->Rp[i]    = fabs(line_rp) * RE_c / UNIT_L;

        state->m        = (double*) realloc(state->m,(i+1)*sizeof(double));
        state->m[i]     = 4.0 / 3.0 * pi_c * pow(state->Rp[i],3) * state->rho[i];

        state->so       = (double**) realloc(state->so,(i+1)*sizeof(double*));
        state->so[i]    = (double*) malloc(DIMS*sizeof(double));
        state->so[i][0] = line_sx * AU_c / UNIT_L;
        state->so[i][1] = line_sy * AU_c / UNIT_L;
        state->so[i][2] = line_sz * AU_c / UNIT_L;

        state->si       = (double**) realloc(state->si,(i+1)*sizeof(double*));
        state->si[i]    = (double*) malloc(DIMS*sizeof(double));
        state->si[i][0] = line_sx * AU_c / UNIT_L;
        state->si[i][1] = line_sy * AU_c / UNIT_L;
        state->si[i][2] = line_sz * AU_c / UNIT_L;

        state->vh       = (double**) realloc(state->vh,(i+1)*sizeof(double*));
        state->vh[i]    = (double*) malloc(3*sizeof(double));
        state->vh[i][0] = 0.0;
        state->vh[i][1] = 0.0;
        state->vh[i][2] = 0.0;

        state->vo       = (double**) realloc(state->vo,(i+1)*sizeof(double*));
        state->vo[i]    = (double*) malloc(DIMS*sizeof(double));
        state->vo[i][0] = line_vx * kms_c / UNIT_V;
        state->vo[i][1] = line_vy * kms_c / UNIT_V;
        state->vo[i][2] = line_vz * kms_c / UNIT_V;

        state->vi       = (double**) realloc(state->vi,(i+1)*sizeof(double*));
        state->vi[i]    = (double*) malloc(DIMS*sizeof(double));
        state->vi[i][0] = line_vx * kms_c / UNIT_V;
        state->vi[i][1] = line_vy * kms_c / UNIT_V;
        state->vi[i][2] = line_vz * kms_c / UNIT_V;

        state->ai       = (double**) realloc(state->ai,(i+1)*sizeof(double*));
        state->ai[i]    = (double*) malloc(DIMS*sizeof(double));
        state->ai[i][0] = 0.0;
        state->ai[i][1] = 0.0;
        state->ai[i][2] = 0.0;
        
	}  
	fclose(file);  

    state->n = i+1;

}

/**
 * Open output stream to file, for writing data
 * @param state     System stucture
 * @param path      Path to output file
 */
FILE* open_writeResults(State_t* state, char* path) {
    FILE* file = fopen(path, "w+");

    if (file == NULL) {
        printf(PRT_ERROR"Could not open output file for writing \n");
        exit(EXIT_FAILURE);
    }

    fprintf(file, "%d \n",state->n);

    return file;
}

/**
 * Write the data describing the state of the system to an output file
 * @param state     System stucture
 * @param file      File stream pointer
 * @param iter      Iteration number
 * @param t         Current time
 * @param dt        Current timestep
 * @param eE        Error in total energy
 * @param LzE       Error in angular momentum around z
 */
void write_state(State_t* state, FILE* file, int iter, double t, double dt,double eE, double LzE) {

    fprintf(
        file,"%d %g %g\n",
        iter, t*UNIT_T, dt*UNIT_T
        );

    fprintf(
        file,"%.10e %.10e %.10e %.10e %.10e %.10e %.10e\n", 
        state->eT*UNIT_E, state->eV*UNIT_E, eE, 
        state->Lx*UNIT_AM, state->Ly*UNIT_AM, state->Lz*UNIT_AM, LzE
        );

    // For each particle
    for (int i = 0; i < state->n; i++) {
        fprintf(
            file, "%s %.8e %.8e %.8e %.8e %.8e %.8e \n",
            state->ps[i],
            state->si[i][0]*UNIT_L,state->si[i][1]*UNIT_L,state->si[i][2]*UNIT_L,
            state->vi[i][0]*UNIT_V,state->vi[i][1]*UNIT_V,state->vi[i][2]*UNIT_V
            );
    }
}