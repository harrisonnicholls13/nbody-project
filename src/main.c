#include "nbody.h" 

int main(int argc, char *argv[]) {
    printf(PRT_MSG"Gravitational n-body simulation by Harrison Nicholls\n");

    // Parameters and constants (all in SI units) -----------------------------
    bool    write =     true;       // Write results to file?
    double  dt_write =  4.0*da_c;   // Time interval at which to write data to file
    int     mod_print = 1e3;        // Frequency at which to print to stdout
    double  tf =        600*yr_c;   // Total integration time
    int     iter_max =  5e7;        // Maximum iterations 
    double  eE_tol =    2e-1;       // Energy error tolerence
    double  LzE_tol =   2e-1;       // Orbital AM error tolerence
    double  dt_sfu =    1.01;       // Factor to scale dt up when increasing step
    double  dt_sfd =    3.00;       // Factor to scale dt down when decreasing step
    double  dt_min =    0.2*da_c;   // Minimum timestep
    double  dt_max =    4.0*da_c;   // Maximum timestep
    bool    to_com =    true;       // Positions such that they are relative to the COM
    bool    adjust_dt = true;       // Adjust timestep?
    bool    doRb =      true;       // Use rollback? (see README for more info)
    double  dt_rb =     20;         // Timestep used initially after rollback
    double  dE_rbr =    5e-1;       // Percentage change in total energy (between iters) sufficient for rollback
    double  eE_rbr =    5e0;        // Percentage total energy error sufficient for rollback (eE_cur always less than this)
    int     temp_N =    1;          // Temp number of particles to allocate

    // System variables ---------------------------------------------------------
    // Initial energies and momenta
    double  eT =        0.0;    // Total kinetic energy (initial)
    double  eV =        0.0;    // Total potential energy (initial)
    double  Lx =        0.0;    // Total angular momentum (initial, xhat)
    double  Ly =        0.0;    // Total angular momentum (initial, yhat)
    double  Lz =        0.0;    // Total angular momentum (initial, zhat)
    double  eS =        0.0;    // Total energy (initial)
    // Error management
    double  eE_cur =    0.0;    // Percentage energy error
    double  eE_old =    0.0;    // Percentage energy error (old)
    double  dE_rbv =    0.0;    // Percentage change in total energy between iters (for rollback)
    double  eE_rbv =    0.0;    // Percentage error in total energy (for rollback)
    bool    didRb =     false;  // Has performed rollback since last print to stdout
    int     rb_count =  0;      // Number of global rollbacks performed
    double  LzE_cur =   0.0;    // Percentage angular momentum error (zhat)
    double  LzE_old =   0.0;    // Percentage angular momentum error (zhat, old)
    // Integration
    double  t =         0.0;    // Current time
    double  dt =        dt_min; // Integration timestep
    int     iter =      0;      // Current iteration number
    char    trend[STRLEN];      // Current integration trend
    FILE*   fOut;               // Stream to results output file
    double  t_wl =      DBL_SML;// Last time data was written to output file

    // Convert variables to natural units
    dt_write /= UNIT_T;
    tf /= UNIT_T;
    dt_min /= UNIT_T;
    dt_max /= UNIT_T;
    dt /= UNIT_T;
    dt_rb /= UNIT_T;

    // Structures containing system state (allocate and initalise)
    State_t* state = (State_t*) malloc(sizeof(State_t));
    if (state == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for system \n");
        exit(EXIT_FAILURE);
    }
    state_init(state,temp_N);

    State_t* s_rbt = (State_t*) malloc(sizeof(State_t));
    if (s_rbt == NULL) {
        printf(PRT_ERROR"Failed to allocate memory for system rollback\n");
        exit(EXIT_FAILURE);
    }
    state_init(s_rbt,temp_N);

    // Open (and read) system configuration file
    printf(PRT_MSG"\n");
    if (argc == 2) {
        read_init(state,argv[1], true);
        if (doRb)
            read_init(s_rbt,argv[1], false);
    } else {
        read_init(state,"./res/solar_system.in", true);
        if (doRb)
            read_init(s_rbt,"./res/solar_system.in", false);
    }
    printf(PRT_MSG"\n");

    // Open file to write simulation history to
    if (write)
        fOut = open_writeResults(state,"./res/history.dat");

    // Calculate initial energies
    eT = calc_eT(state);
    eV = calc_eV(state);
    eS = eT+eV;
    Lx = calc_Lx(state);
    Ly = calc_Ly(state);
    Lz = calc_Lz(state);

    state->eV = eV;
    state->eT = eT;
    state->eS = eS;
    state->Lx = Lx;
    state->Ly = Ly;
    state->Lz = Lz;

    // Calculate total mass of system
    for (int i = 0; i < state->n; i++) {
        state->m_tot += state->m[i];
    }

    // Make copy of state for rollback
    if (doRb)
        state_cpy(state, s_rbt);

    // Write initial state of system to a file
    write_state(state,fOut,iter,t,dt, eE_cur, LzE_cur);

    // Integrate system over time
    printf(PRT_MSG"Starting integration... \n");
    while(t < tf) {

        if (iter >= iter_max) {
            printf(PRT_WARN"Maximum iterations reached; iter = %d \n",iter);
            break;
        }

        // Calculate energies, energy error, angular momentum, AM error
        state->eT = calc_eT(state);
        state->eV = calc_eV(state);
        state->eS = state->eT + state->eV;
        state->Lx  = calc_Lx(state);
        state->Ly  = calc_Ly(state);
        state->Lz  = calc_Lz(state);
        eE_old  = eE_cur;
        eE_cur  = DBL_PCT_CNG(state->eS, eS);
        LzE_old = LzE_cur;
        LzE_cur = DBL_PCT_CNG(state->Lz, Lz);

        // Calculate new timestep if this feature is enabled
        if (adjust_dt) {

            // Change timestep accordingly
            if ( (eE_cur > eE_tol) || (LzE_cur > LzE_tol)) {
                // Decrease timestep if error is above tolerence
                strcpy(trend,"decrease");
                dt /= dt_sfd;
            } else if ( (eE_cur <= eE_old) && (LzE_cur <= LzE_old) ) {
                // Increase timestep, as long as errors are not increasing
                strcpy(trend,"increase");
                dt *= dt_sfu;
            }  else {
                // Otherwise, keep timestep constant
                strcpy(trend,"constant");
            }

            // If timestep is above maximum then set it equal to maximum value
            if (dt >= dt_max) {
                dt = dt_max;
                strcpy(trend,"maximum");
            }

            // If timestep is below minimum then it equal to minimum value
            if ( dt < dt_min) {
                dt = dt_min;
                strcpy(trend,"minimum");
            }

            // Integrate step forward and/or perform rollback if appropriate
            if (doRb) {
                state_cpy(state, s_rbt);
                iterate_velVerlet(s_rbt,dt);
                s_rbt->eT = calc_eT(s_rbt);
                s_rbt->eV = calc_eV(s_rbt);
                s_rbt->eS = s_rbt->eT + s_rbt->eV;
                dE_rbv  = DBL_PCT_CNG(state->eS,s_rbt->eS); 
                eE_rbv  = DBL_PCT_CNG(s_rbt->eS,eS);
                if (dE_rbv >= dE_rbr || eE_rbv >= eE_rbr) {
                    dt = dt_rb;
                    didRb = true;
                    rb_count++;
                }
            }

        } else {
            strcpy(trend,"fixed");
            dt = dt_min;
        }

        // Integrate step
        iterate_velVerlet(state,dt);
        calc_com(state, dt);

        // Adjust velocities by COM velocity
        if (to_com){ 
            adjust_v_com(state);
        }

        // Iterate counters
        iter+=1;
        t += dt;

        // Print current state
        if ((iter % mod_print) == 0) {
            printf(PRT_MSG"********** Iteration %7d *********** \n",iter);
            if (mod_print > 0) {

                if (didRb) {
                    strcat(trend,", rb");
                    didRb = false;
                }

                printf(PRT_MSG"t  = %+.4e     dt    = %+.4e \n",t*UNIT_T,dt*UNIT_T);
                printf(PRT_MSG"eS = %+.4e     eE    = %+.4e \n",state->eS*UNIT_E,eE_cur);
                printf(PRT_MSG"Lz = %+.4e     LzE   = %+.4e \n",state->Lz*UNIT_AM,LzE_cur);
                printf(PRT_MSG"β%% = %+.4e     trend = %s \n",state->v_max/3e6*UNIT_V*100,trend);
                // printf(PRT_MSG"com_v = {%+.4e %+.4e %+.4e}\n",state->com_vi[0]*UNIT_V,state->com_vi[1]*UNIT_V,state->com_vi[2]*UNIT_V);
                printf(PRT_MSG"\n");
            }
        }
        // Save data to file if required
        if (write && ( (t - t_wl) >= dt_write)) {
            write_state(state,fOut,iter,t,dt, eE_cur, LzE_cur);
            t_wl = t;
        } 
    }

    printf(PRT_MSG"********* Integration complete ********* \n");
    printf(PRT_MSG"Total iterations: %d \n",iter);
    printf(PRT_MSG"Final time:       %.1f years\n", (t/yr_c*UNIT_T));
    printf(PRT_MSG"Num of rollbacks: %d \n",rb_count);

    // Tidy up and exit
    if (write) {
        fflush(fOut);
        fclose(fOut);
    }
    state_free(state);
    state_free(s_rbt);
    
    printf(PRT_MSG"Goodbye\n");
    return EXIT_SUCCESS;
}
