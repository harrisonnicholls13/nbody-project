#include "nbody.h"

/**
 * Calculate the total kinetic energy of the system
 * @param s     System stucture
 * @return      Total kinetic energy
 */
double calc_eT(State_t* s) {
    double ek = 0.0;
    for (int i = 0; i < s->n; i++) {
        ek += ( s->m[i] * (s->vi[i][0]*s->vi[i][0] + s->vi[i][1]*s->vi[i][1] + s->vi[i][2]*s->vi[i][2]) );
    }
    ek *= 0.5;
    return ek;
}

/**
 * Calculate the total gravitational potential energy of the system
 * @param s     System stucture
 * @return      Total potential energy
 */
double calc_eV(State_t* s) {
    double ev = 0.0;
    double dx, dy, dz;
    for (int i = 0; i < s->n; i++) {
        for (int j = i+1; j < s->n; j++) {
            dx = s->si[i][0] - s->si[j][0];
            dy = s->si[i][1] - s->si[j][1];
            dz = s->si[i][2] - s->si[j][2];
            ev -= s->m[i] * s->m[j] * pow(dx*dx + dy*dy + dz*dz,-0.5);
        }
    }
    ev *= G_c;
    return ev;
}

/**
 * Calculate the angular momentum of the system about the vector x-hat
 * @param s     System stucture
 * @return      Total angular momentum (x-hat)
 */
double calc_Lx(State_t* s) {
    double lx= 0.0;
    for (int i = 0; i < s->n; i++) {
        lx+= s->m[i] * (s->si[i][1]*s->vi[i][2] - s->si[i][2]*s->vi[i][1]);
    }
    return lx;
}

/**
 * Calculate the angular momentum of the system about the vector y-hat
 * @param s     System stucture
 * @return      Total angular momentum (y-hat)
 */
double calc_Ly(State_t* s) {
    double ly= 0.0;
    for (int i = 0; i < s->n; i++) {
        ly += s->m[i] * (s->si[i][2]*s->vi[i][0] - s->si[i][0]*s->vi[i][2]);
    }
    return ly;
}

/**
 * Calculate the angular momentum of the system about the vector z-hat
 * @param s     System stucture
 * @return      Total angular momentum (z-hat)
 */
double calc_Lz(State_t* s) {
    double lz= 0.0;
    for (int i = 0; i < s->n; i++) {
        lz += s->m[i] * (s->si[i][0]*s->vi[i][1] - s->si[i][1]*s->vi[i][0]);
    }
    return lz;
}

/**
 * Calculate the position and velocity of the centre-of-mass of the system
 * @param s     System stucture
 * @param dt    Most recent timestep
 */
bool calc_com(State_t* state, double dt) {

    for (int j = 0; j < DIMS; j++) { // for each dimension

        // Get position of COM
        state->com_so[j] = state->com_si[j];
        state->com_si[j] = 0.0;
        for (int i = 0; i < state->n; i++) {
            state->com_si[j] += state->si[i][j] * state->m[i];
        }
        state->com_si[j] /= state->m_tot;

        // Get velocity of COM
        state->com_vi[j] = (state->com_si[j] - state->com_so[j]) / dt;

    }

    return EXIT_SUCCESS;
}

/**
 * Subtract COM position from body positions to keep the system at the origin
 * @param state     System stucture
 */
bool adjust_v_com(State_t* state) {
    // See this StackExchange post for more information:
    // https://physics.stackexchange.com/questions/217502/center-of-mass-chaotic-in-n-body-gravitational-interactions

    for (int j = 0; j < DIMS; j++) {
        for (int i = 0; i < state->n; i++) {
            state->vi[i][j] -= state->com_vi[j];
        }
    }
    return EXIT_SUCCESS;
}

/**
 * (Private) Calculate the acceleration on a particle
 * @param state     System stucture
 * @param i         Index of particle to calculate acceration of
 */
bool _solve_accel(State_t* state, int i) {

    double aa   = 0;    // Iteration total force
    double dx   = 0;    // Distance in x
    double dy   = 0;    // Distance in y
    double dz   = 0;    // Distance in z
    double dr   = 0;    // Distance magnitude

    // Reset accel sums
    state->ai[i][0] = state->ai[i][1] = state->ai[i][2]= 0.0f;

    // Loop over other particles
    for (int j = 0; j < state->n; j++) {
        if (j == i) continue;

        // Work out relative positions of particles
        dx = state->si[i][0] - state->si[j][0];
        dy = state->si[i][1] - state->si[j][1];
        dz = state->si[i][2] - state->si[j][2];
        dr = sqrt(dx*dx + dy*dy + dz*dz);

        // record min distance
        if (dr < state->r_min) {
            state->r_min = dr;
        }

        // Calculate acceleration
        if (dr < (state->Rp[i] + state->Rp[j])){
            // If planets intersect, treat one as sphere and other as point-like
           aa = -4.0/3.0 * G_c *  pi_c * dr * state->rho[j];
        } else {
           // If planets don't intersect
            aa = -1.0 * state->m[j] * G_c / (dr*dr);
        }

        // Find components of acceleration
        state->ai[i][0] += aa * dx / dr;
        state->ai[i][1] += aa * dy / dr;
        state->ai[i][2] += aa * dz / dr;

    }

    return EXIT_SUCCESS;
}

/**
 * Iterate the solver by a given step using the Velocity Verlet method
 * @param state     System stucture
 * @param h         Integration step size
 */
bool iterate_velVerlet(State_t* state, double h) {

    // Current maximum velocity across all particles
    state->v_max = DBL_SML;
    double v_max_temp;
    state->r_min = DBL_BIG;

    // Copy current positions/velocities to 'old' positions/velocities
    copy2D(state->si,state->so,state->n,DIMS);
    copy2D(state->vi,state->vo,state->n,DIMS);

    // For each particle
    for (int i = 0; i < state->n; i++) {
        if (state->alive[i] == false) {
            // Don't simulate this particle
            continue;
        }
        // For each dimension iterate forwards
         _solve_accel(state,i);
        for (int j = 0; j < DIMS; j++) {
            state->vh[i][j] = state->vo[i][j] + 0.5 * h * state->ai[i][j];
            state->si[i][j] = state->so[i][j] +       h * state->vh[i][j];
        }
        _solve_accel(state,i);
        for (int j = 0; j < DIMS; j++) {
            state->vi[i][j] = state->vh[i][j] + 0.5 * h * state->ai[i][j];
        }

        // record max velocity
        v_max_temp = pow(state->vi[i][0]*state->vi[i][0] + state->vi[i][1]*state->vi[i][1] +state->vi[i][2]*state->vi[i][2],0.5);
        if (v_max_temp > state->v_max)
            state->v_max = v_max_temp;

    }

    return EXIT_SUCCESS;
}