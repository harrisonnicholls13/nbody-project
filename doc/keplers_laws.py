#!/usr/bin/env python3

# This script uses plots to show that my code can demonstrate Kepler's 3 laws of planetary motion.
# Kepler's 1st law:
#   "The orbit of every planet is an ellipse with the Sun at one of the two foci."
# Kepler's 2nd law:
#   "A line joining a planet and the Sun sweeps out equal areas during equal intervals of time."
#   dA/dt = πab/T
# Kepler's 3rd law:
#   "The ratio of the square of an object's orbital period to the cube of its semi-major axis is equal for objects orbiting the same primary."
#   a^3/T^2 ~= GM/(4π^2)

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager as mpfm
import matplotlib.animation as ani
from skimage.measure import EllipseModel
from matplotlib.patches import Ellipse

fs = 18
mpl.rc('text',usetex=False)    
mpl.rc('font',family='sans-serif',size=fs)
prop = mpfm.FontProperties(size=fs)

da_c = 86400
yr_c = 3.154e7
au_c = 1.496e11
dpi  = 124

cmap =  mpl.cm.get_cmap('rainbow')

# Params
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/w12_project/'
fdata = fpath + 'res/history.dat'

# Read and process data
file = open(fdata,'r')
fdat = file.read().splitlines()
file.close()

num_part = int(fdat[0])
pitch = num_part+2
num_iter = int((len(fdat)-1)/pitch)

data = []
t_arr = []
dt_arr = []
i_arr = []
s_arr = []
for i in range(0,num_iter*pitch,pitch):
    idat = []
    ls = fdat[i]
    for j in range(0,pitch,1):
        pdat = {}
        line = fdat[i+j+1].split()
        if (j == 0):
            # Add iteration to tracker
            i_arr.append( int(line[0]))
            t_arr.append( float(line[1]))
            dt_arr.append(float(line[2]))
        elif (j == 1):
            # Store energies
            pdat["name"] = "_energies_"
            pdat["kin"]  = float(line[0]) # kin
            pdat["pot"]  = float(line[1]) # pot
            pdat["err"]  = float(line[2]) # err
            idat.append(pdat)
        else:
            # Store particle data
            if (len(s_arr) != num_part):
                s_arr.append(line[0])
            pdat["name"] = line[0] # particle name
            pdat["sx"]   =  float(line[1])     # sx
            pdat["sy"]   =  float(line[2])     # sy
            pdat["sz"]   =  float(line[3])     # sz
            pdat["vx"]   =  float(line[4])     # vx
            pdat["vy"]   =  float(line[5])     # vy
            pdat["vz"]   =  float(line[6])     # vz
            idat.append(pdat)
            
    data.append(idat)

# Code below plots T^2 vs A^3 for each body in order to demonstrate Kepler's 3rd law.

# Find time interval and indices
t_i = 0      # Start target
t_f = 60*yr_c    # End target
i_i = -1
i_f = -1
for i,t in enumerate(t_arr):
    if (t>=t_i):
        i_i = i
        t_i = t
        break

for i,t in enumerate(t_arr):
    if (t>=t_f):
        i_f = i
        t_f = t
        break

if (i_f == -1):
    print("Could not find end point")

T  = [] # Orbital period
T2 = [] # Period squared
A3 = [] # SMA cubed
Ti = [] # Index of time corresponding to T
ex_arr = []
ey_arr = []
ea_arr = []
eb_arr = []
et_arr = []

print("\nDetermine ellipse and A3...")

# For each body determine A^3 and orbital parameters
for b in range(num_part):
    print("Particle: %3d (%s)" % (b,data[0][b+1]["name"]))
    points = []
    # Collect data over time, between bounds
    for v in data[i_i:i_f]:        
        p = ( v[b+1]["sx"], v[b+1]["sy"])
        points.append(p)

    # Fit data
    ell = EllipseModel()
    est_succ = ell.estimate(np.asarray(points))
    if (est_succ == False):
        print("Could not estimate orbit as an ellipse. Consider using a larger time interval.")
        exit()
        
    xc, yc, a, b, theta = list(ell.params)
    a *= 2
    b *= 2
    ex_arr.append(xc)
    ey_arr.append(yc)
    ea_arr.append(a)
    eb_arr.append(b)
    et_arr.append(theta)

    print("axes = %.3e, %.3e" % (a,b))

    A3.append(max(a,b,)**3)

# Function to get angle phi given x,y and ellipse focus
def getPhi(x,y,xc,yc):
    dx = x - xc
    dy = y - yc
    Q = np.abs(dy/dx)

    if (dx>0) and (dy>0):
        return np.arctan(Q)
    elif (dx<0) and (dy>0):
        return (np.pi - np.arctan(Q))
    elif (dx<0) and (dy<0):
        return (np.pi + np.arctan(Q))
    elif (dx>0) and (dy<0):
        return (2*np.pi - np.arctan(Q))


print("\nDetermine T2...")

# For each body determine T^2 by measuring how long it takes to complete 2pi radians
tol = 45 # tolerence for orbit crossing
tol = tol * np.pi / 180
for b in range(num_part):
    print("Particle: %d (%s)" % (b,data[0][b+1]["name"]))
    # Collect data over time, between bounds
    t_cur = 0.0
    i = 1
    succ = False
    t0 = -1
    phi = getPhi(data[i][b+1]["sx"],data[i][b+1]["sy"],ex_arr[b],ey_arr[b])
    while (i<num_iter):
        phi0 = phi
        phi = getPhi(data[i][b+1]["sx"],data[i][b+1]["sy"],ex_arr[b],ey_arr[b])
        t_cur = t_arr[i]

        if (phi0 < tol) and (phi > (2*np.pi-tol)):
            passed = True
        elif (phi < tol) and (phi0 > (2*np.pi-tol)):
            passed = True
        else:
            passed = False

        if (passed): # Passed axis
            if (t0 == -1): # First pass
                t0 = t_cur
            else:
                t_period = t_cur - t0
                succ = True
                break
        i+=1

    if (succ == False):
        print("Could not estimate orbital period of planet. Maybe it did not complete 1 whole orbit?")
        print("t0    = ", t0)
        print("t_cur = ", t_cur)
        exit()

    print("\t Period = %.3g yrs" % (t_period / yr_c))

    T2.append(t_cur**2)
    T.append(t_cur)
    Ti.append(i)


# Plot body and its fitted ellipse to show Kepler's 1st law

fig, ax = plt.subplots(1,1,figsize=(8,5))
fig.tight_layout()
for bdy in range(num_part):
    x = []
    y = []
    for i in range(i_i,i_f):
        x.append(data[i][bdy+1]["sx"]/au_c)
        y.append(data[i][bdy+1]["sy"]/au_c)
    color = cmap(bdy/num_part)
    ax.plot(x,y,lw=2,color='black',linestyle='dashed')
    ax.scatter(ex_arr[bdy]/au_c,ey_arr[bdy]/au_c,color=color,s=60,marker='x')
    ell_patch = Ellipse((np.asarray(ex_arr[bdy])/au_c, np.asarray(ey_arr[bdy])/au_c), ea_arr[bdy]/au_c, eb_arr[bdy]/au_c, et_arr[bdy]*180/np.pi, edgecolor=color, facecolor='none',lw=4,label=data[0][bdy+1]["name"])
    ax.add_patch(ell_patch)
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax.set_ylabel("y position [AU]")
ax.set_xlabel("x position [AU]")
plt.show()
# fig.savefig(fpath + "doc/img/fig_keplers_1st_law.png",dpi=dpi,bbox_inches='tight')

# Plot T2 and A3 to show Kepler's 3rd law
fig,ax = plt.subplots(1,1,figsize=(10,8))
ax.set_xscale("log")
ax.set_xlabel("T^2 [s^2]")
ax.set_yscale("log")
ax.set_ylabel("A^3 [m^3]")


ax.scatter(T2[1:],A3[1:],label="Simulation",color='black')
for i in range(1,len(T2)):
    ax.text(T2[i],A3[i],s_arr[i],fontsize="small")

theory_x = np.linspace(0,2e20,10)
theory_y = [ (6.67e-11 * 2e30 / (4*np.pi) * x) for x in theory_x]
ax.plot(theory_x,theory_y,color='red',lw=2,label="Theory")
plt.legend()

plt.show()
fig.savefig(fpath + "doc/img/fig_keplers_3rd_law.png",dpi=dpi,bbox_inches='tight')


# Function to get the area of a triangle given its vertices and centre
def tri_area(x1, y1, x2, y2, x3, y3):
    area = 0.5 * (  x1*(y2 - y3) + x2*(y3 - y1) + x3*(y1 - y2))
    return abs(area)



print("\nKepler's 2nd law analysis...")

# Plot many (dA,dt) for each planet to demonstrate that it should be linear, for Kepler's 2nd law
segs = 9000 # How many iterations to generate segments from
gs   = 20  # How many segments form each group
fig,ax = plt.subplots(1,1,figsize=(10,8))
for b in range(1,num_part):
    pname = data[0][b+1]["name"]
    print("Particle: %3d (%s)" % (b,pname))
    
    if (segs > num_iter):
        print("Too many segments!")
        segs = num_iter-gs-1

    xc = ex_arr[b]
    yc = ey_arr[b]

    areas = []
    times = []

    # For each segment calculate area and time
    for i in range(segs):
        x0 = data[i  ][b]["sx"]
        x1 = data[i+1][b]["sx"]
        y0 = data[i  ][b]["sy"]
        y1 = data[i+1][b]["sy"]
        
        da = tri_area(x0,y0,x1,y1,xc,yc)
        dt = t_arr[i+1] - t_arr[i]
        
        areas.append(da)
        times.append(dt)
        # times.append(t_arr[i])

    # Combine adjacent segments together
    # si = 0 # seg counter
    # gi = 0 # group counter
    # areas_grp = []
    # times_grp = []
    # while (si < segs):
    #     a_acc = 0
    #     t_acc = 0
    #     for j in range(gs):
    #         a_acc += areas[si]
    #         t_acc += times[si]
    #         si += 1
    #     areas_grp.append(a_acc)
    #     times_grp.append(t_acc)
    #     gi += gs

    times_grp = times
    areas_grp = areas

    # Sort into ascending timestep-length order
    mask = np.argsort(times_grp)
    times_sorted = []
    areas_sorted = []
    for m in mask:
        times_sorted.append(times_grp[m]/da_c)
        areas_sorted.append(areas_grp[m])

    # Plot data for this planet
    color = cmap(b/num_part)
    ax.scatter(times_sorted, areas_sorted,label=pname,color=color,s=20)

    gradient = ea_arr[b]*eb_arr[b]*np.pi/T[b]
    gradient = 1.496e11 * 1.496e11 * np.pi / (3.1558e7)
    theory_x = times_sorted
    theory_y = [t*gradient*da_c for t in times_sorted]
    ax.plot(theory_x,theory_y,lw=4,color='black')
    ax.plot(theory_x,theory_y,lw=2,color=color)

ax.set_xlabel("Time interval [days]")
ax.set_ylabel("Segment area [m^2]")
plt.legend()
plt.show()
# fig.savefig(fpath + "doc/img/fig_keplers_2nd_law.png",dpi=dpi,bbox_inches='tight')
