#!/usr/bin/env python3

# This code plots data from the simulation over time, as an animation. It shows positions of the bodies, with trails to show their
# recent history, as well as a plot of energy (and its components) over time, at the same time.

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager as mpfm
import matplotlib.animation as ani

fs = 18
mpl.rc('text',usetex=False)    
mpl.rc('font',family='sans-serif',size=fs)
prop = mpfm.FontProperties(size=fs)
cmap =  mpl.cm.get_cmap('rainbow')

au = 1.496e+11 # au
yr = 3.154e+7  # year
da = 86400     # day
sol_am = 1.92e41

# Params
fpath = '/home/harrison/Documents/uni/Year 4/Computational Physics And Modelling/w12_project/'
fdata = fpath + 'res/history.dat'
frames = 131
xlims = 25
ylims = 25
td = 5  # Tick distance [AU]
zlims = 1
tlen = 130  # Trail length
axis_xcomp = "x"
axis_ycomp = "y"
ss = 93  # Scatter size

# Read and process data
file = open(fdata,'r')
fdat = file.read().splitlines()
file.close()

num_part = int(fdat[0])
pitch = num_part+2
num_iter = int((len(fdat)-1)/pitch)

if (frames >= num_iter):
    step = 1
else:
    step = int(num_iter/frames)

print("Step size  = %d" % step)

data = []
t_arr = []
dt_arr = []
i_arr = []
s_arr = []
for i in range(0,num_iter*pitch,pitch):
    idat = []
    ls = fdat[i]
    for j in range(0,pitch,1):
        pdat = {}
        line = fdat[i+j+1].split()
        if (j == 0):
            # Add iteration to tracker
            i_arr.append( int(line[0]))
            t_arr.append( float(line[1])/yr)
            # print(float(line[1])/yr)
            dt_arr.append(float(line[2])/da)
        elif (j == 1):
            # Store energies
            pdat["name"] = "_energies_"
            pdat["kin"]  = float(line[0]) # kin
            pdat["pot"]  = float(line[1]) # pot
            pdat["err"]  = float(line[2]) # err
            pdat["Lx"]   = float(line[3])/sol_am # AM (xhat)
            pdat["Ly"]   = float(line[4])/sol_am # AM (yhat)
            pdat["Lz"]   = float(line[5])/sol_am # AM (zhat)
            pdat["LzE"]  = float(line[6]) # AM error (zhat)
            idat.append(pdat)
        else:
            # Store particle data
            if (len(s_arr) != num_part):
                s_arr.append(line[0])
            pdat["name"] = line[0] # particle name
            pdat["sx"]   = (float(line[1]))/au # sx
            pdat["sy"]   = (float(line[2]))/au # sy
            pdat["sz"]   = (float(line[3]))/au # sz
            pdat["vx"]   =  float(line[4])     # vx
            pdat["vy"]   =  float(line[5])     # vy
            pdat["vz"]   =  float(line[6])     # vz
            idat.append(pdat)
            
    data.append(idat)

# Plot results in animation

fs = 13

art = []
colors = [cmap(i/num_part) for i in range(num_part)]

fig,(ax1,ax2) = plt.subplots(1,2,figsize=(17.8,7.3))
ax1.set_xlabel(axis_xcomp + " position [AU]")
ax1.set_ylabel(axis_ycomp + " position [AU]")
ax1.set_facecolor((0.02, 0.02, 0.02))

# Work out ticks and limits
xticks = [0]
i = 0
xt = 0
while (xt<xlims):
    xt = i*td
    xticks.append(xt)
    xticks.insert(0,-1*xt)
    i += 1

yticks = []
i = 0
yt = 0
while (yt<ylims):
    yt = i*td
    yticks.append(yt)
    yticks.insert(0,-1*yt)
    i += 1

zticks = []
i = 0
zt = 0
while (zt<zlims):
    zt = i*td
    zticks.append(zt)
    zticks.insert(0,-1*zt)
    i += 1

if (axis_xcomp == "x"):
    ax1.set_xlim([-xlims,xlims])
    ax1.set_xticks(xticks)
    a1 = ax1.text(-xlims*0.98,xlims*1.05,"Time")
elif (axis_xcomp == "y"):
    ax1.set_xlim([-ylims,ylims])
    ax1.set_xticks(yticks)
    a1 = ax1.text(-ylims*0.98,ylims*1.1,"Time")
elif (axis_xcomp == "z"):
    ax1.set_xlim([-zlims,zlims])
    ax1.set_xticks(zticks)
    a1 = ax1.text(-zlims*0.98,zlims*1.05,"Time")

if (axis_ycomp == "x"):
    ax1.set_ylim([-xlims,xlims])
    ax1.set_yticks(xticks)
elif (axis_ycomp == "y"):
    ax1.set_ylim([-ylims,ylims])
    ax1.set_yticks(yticks)
elif (axis_ycomp == "z"):
    ax1.set_ylim(zticks)
    ax1.set_yticks(np.arange(-zlims,zlims+td,td))

ax1.grid(linewidth=1,color=(0.3,0.3,0.3))

art.append(a1)
for i in range(num_part):
    a1 = ax1.scatter([0],[1],color="white",s=ss) # Planet dots (background)
    a2 = ax1.scatter([0],[1],color=colors[i],s=ss*0.6) # Planet dots
    a3 = ax1.text(0,0,s_arr[i],fontsize=fs,color="white")   # Planet names
    a4 = ax1.plot([0],[1],color=colors[i],linewidth=3.6,alpha=0.9)[0]    # Planet trails
    art.append(a1)
    art.append(a2)
    art.append(a3)
    art.append(a4)
    
# Set up energy plot
ax2.set_xlim([0,10])
ax2.set_xlabel("Time [yr]")
ax2.set_ylabel("Energy [J]")

# Legend handles
handles = []

# Plot energy
eT = ax2.plot(t_arr,[d[0]['kin'] for d in data],color='orangered',lw=1.5,label="Kin energy")[0]
art.append(eT)
handles.append(eT)
eV = ax2.plot(t_arr,[d[0]['pot'] for d in data],color='aquamarine',lw=1.5,label="Pot energy")[0]
art.append(eV)
handles.append(eV)
eS_dat = [d[0]['pot']+d[0]['kin'] for d in data]
eS = ax2.plot(t_arr,eS_dat,color='black',lw=1.5,label="Tot energy")[0]
art.append(eS)
handles.append(eS)


# Set up AM plot
ax3 = ax2.twinx()
ax3.tick_params(axis='y',labelcolor='purple')
ax3.set_ylabel("L_z [AM☉]",color='purple')

# Plot AM
LM_dat = []
for d in data:
    Lx_i = d[0]['Lx']**2
    Ly_i = d[0]['Ly']**2
    Lz_i = d[0]['Lz']**2
    LM_dat.append(np.sqrt(Lx_i + Ly_i + Lz_i))

Lz = ax3.plot(t_arr,[d[0]['Lz'] for d in data],color='purple',lw=1.5,label="Orbital AM")[0]
art.append(Lz)
handles.append(Lz)

ax2_leg = ax2.legend(handles=handles,loc="upper left", facecolor="white")

# Helper function to get previous data, for trails
def getPrev(arr,i,c):
    iH = int(min(i+1,len(arr)))
    iL = int(max(0,i-c))
    return arr[iL:iH]

# Timestamps
frames = np.arange(0,len(t_arr),step)

# Print info before animation begins
print("Particles  = %d" % num_part)
print("Iterations = %d" % num_iter)
print("Frames     = %d" % len(frames))
print("") # don't remove

# Animate a frame of the animation
def animate(i):
    # Clear output
    print ("\033[A                             \033[A")
    
    t = t_arr[i]
    
    print("Progress: %d / %d (%.1f%%)" % ( i +step, len(t_arr), float(i+step)/len(t_arr)*100))

    ft_yrs = int(t)
    ft_das = int( (t-ft_yrs) * yr / da )
    txt = "Time: %04dyr %03dda, Timestep: %2.2g da" % (ft_yrs,ft_das,dt_arr[i])
    art[0].set_text(txt)
    
    c = 1
    for j in np.arange(1,num_part*4,4):
            
        # Adjust scatter data
        di = data[i]
        x = di[c]["s"+axis_xcomp]
        y = di[c]["s"+axis_ycomp]
        art[j].set_offsets([x,y])
        art[j+1].set_offsets([x,y])

        # Adjust text
        art[j+2].set_position((x,y))

        # Adjust trails 
        if (tlen > 0):
            xt_arr = []
            yt_arr = []
            for k in range(len(t_arr)):
                xt_arr.append(data[k][c]["s"+axis_xcomp])
                yt_arr.append(data[k][c]["s"+axis_ycomp])
            xt = getPrev(xt_arr,i,tlen)
            yt = getPrev(yt_arr,i,tlen)
            art[j+3].set_data(xt,yt)
        
        c += 1

    # Adjust ax2 and ax3 lims
    # The +1e-7 is there to avoid errors when t=0
    ax2.set_xlim([0.7*t,t+1e-7])
    ax3.set_xlim([0.7*t,t+1e-7])

    return art


animate(frames[-2])
plt.show()

# Do animation
# anim = ani.FuncAnimation(fig,animate,interval=100,blit=True,frames=frames)
# anim.save(fpath + "doc/img/anim_RECENT.mp4", fps=16, dpi=110,extra_args=['-vcodec', 'libx264'])
# print("Done!")

