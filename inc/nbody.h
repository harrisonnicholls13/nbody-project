#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <limits.h>

// Prefixes to print messages (with colors!)
#define PRT_SPACE		    "---------  "
#define PRT_MSG			    "\033[0;32m[Message]  \033[0m"  // General message
#define PRT_PRMPT		    "\033[0;36m[Command]  \033[0m"  // Prompt
#define PRT_QUERY		    "\033[0;34m[ Query ]  \033[0m"  // Query for input
#define PRT_WARN		    "\033[0;33m[Warning]  \033[0m"  // Warning 
#define PRT_ERROR		    "\033[0;31m[ Error ]  \033[0m"  // Error
#define STRLEN              128

// Macros for neater code
#define STREQ(_s,_x)              ( strcmp(_s,_x) == 0 ? 1 : 0)      // Checks if strings are equal
#define EVEN(_xv)                 ( ((_xv) % 2) == 0 ? 1 : 0)        // Check if x is even
#define DBL_PCT_CNG(_fnl,_ini)    ( fabs( ((_fnl) - (_ini)) / (_ini) * 100 ))   // Calculate percentage change in a value (for doubles)

// Constants and units
#define DBL_BIG (1e300)         // Big value for a double
#define DBL_SML (1e-300)        // Small value for a double
#define G_c     (1.0)           // Gravitational constant (m3.kg-1.s-2)
#define pi_c    (3.1415926536)  // Pi
#define RE_c    (6.371e6)       // Earth radius (metres)
#define MJ_c    (1.898e27)      // Jupiter mass
#define rho_c   (1000.0)        // Density unit conversion (from g.cm-3 to kg.m-3)
#define AU_c    (1.496e+11)     // Astronomical unit (metres)
#define kms_c   (1000.0)        // Orbital velocity unit conversion ( from km/s to m/s)
#define yr_c    (3.154e+7)      // 1 year in seconds
#define da_c    (86400)         // 1 day in seconds

// Code units
#define DIMS    (3)
#define UNIT_L  AU_c
#define UNIT_M  MJ_c
#define UNIT_T  (162656353.1)
#define UNIT_V  (919.7304)
#define UNIT_E  (1.60552e33)
#define UNIT_AM (2.611489983e41)


// Define guard
#ifndef GUARD
#define GUARD

// Struct to contain information about the current state of the system (SI units)
typedef struct state {
    // Particle info
    int         n;          // Number of particles
    double      v_max;      // Maximum velocity of particles
    double      r_min;      // Minimum particle distance
    char**      ps;         // Particle names
    bool*       alive;      // Particle alive? (true/false)
    double*     rho;        // Particle densities
    double*     Rp;         // Particle radii
    double*     m;          // Particle mass (calculated)
    double      m_tot;      // Total mass of particles
    double**    so;         // Positions of particles at each interval (old)
    double**    si;         // Positions of particles at each interval
    double**    vo;         // Velocities of particles at each interval (old)
    double**    vh;         // Velocities of particles (half interval)
    double**    vi;         // Velocities of particles at each interval
    double**    ai;         // Accelerations of particles at each interval
    double*     com_so;     // Position of centre of mass (old)
    double*     com_si;     // Position of centre of mass
    double*     com_vi;     // Velocity of centre of mass
    // Energies
    double      eV;         // Potential energy
    double      eT;         // Kinetic energy
    double      eS;         // Total energy
    // Angular momenta
    double      Lx;         // Angular momentum around vector x-hat
    double      Ly;         // Angular momentum around vector y-hat
    double      Lz;         // Angular momentum around vector z-hat
} State_t;

// Prototype functions....
double      frand(double min, double max);
double      getTime(void);
double**    malloc2D(int n1, int n2);
void        free2D(double** v, int n1);
void        copy2D(double** src, double** dst, int n1, int n2);
void        state_init(State_t* s, int temp_N);
void        state_cpy(State_t* src, State_t* dst);
void        state_free(State_t* s);
void        read_init(State_t* s, char* path, bool print);
double      calc_eT(State_t* s); 
double      calc_eV(State_t* s); 
double      calc_Lx(State_t* s); 
double      calc_Ly(State_t* s); 
double      calc_Lz(State_t* s); 
bool        calc_com(State_t* state, double dt);
bool        adjust_v_com(State_t* state);
bool        iterate_velVerlet(State_t* state, double h);
FILE*       open_writeResults(State_t* state, char* path);
void        write_state(State_t* state, FILE* file, int iter, double t, double dt, double eE, double LzE);

#endif