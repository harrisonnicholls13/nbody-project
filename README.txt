README file for gravitational n-body simulation code
Written by Harrison Nicholls (hen204@exeter.ac.uk)


## Introduction
The program takes in the initial state of a gravitational system (e.g. 'solar_system.in'), and iterates forwards until the final time is reached.
During this process, it outputs the state of the system at regular intervals to a file 'history.dat' This history file is read by a python 
script 'animate.py' to animate the results, and 'keplers_laws.py' to plot graphs to demonstrate Kepler's laws of planetary motion.


## Compilation and run
Compile the program by running `make` in the folder containing the Makefile.
Run the program by executing `./bin/nbody <scf>` in the same folder as above, where <scf> is the System Configuration File (see next section).


## The System Configuration File
By default, this file is called 'solar_system.in' and is stored in './res/'. Alternative files can be loaded by running the program with 
the file name (and path) as its first command line argument.
The code assumes that the first line in this file contains column headers, so do not remove it. This is very important.
Each row in the file corresponds to a different gravitational body.
Each row contains information about each body - each column in a row is separated by whitespace. The columns are as follows:
 * Name:             The name of the body, with no spaces           N/A         (String)
 * Alive:            Flag for allowing this body to move (t or f)   N/A         (Boolean)
 * Density:          Average mass-density of the body               g.cm^-3     (Float)
 * Radius:           Radius of the body                             Earth radii (Float)
 * Radial position:  Radial distance from the origin                AU          (Float)
 * Azimuthal angle:  Angular position from y=0,z=0 anticlockwise    Radians     (Float)
 * Polar position:   Polar angle measured from the x-y plane        Radians     (Float)
 * Radial velocity:  Radial velocity directed towards the origin    km/s        (Float)
 * Orbital velocity: Velocity tangent to the current orbit          km/s        (Float)
 * Polar velocity:   Velocity in the direction of the polar angle   km/s        (Float)


## Simulation
The code simulates all of the bodies read from the System file, and each is treated equally. It uses the Velocity Verlet method for integrating
the system. The simulation finishes when the time `t` has reached the total integration time, or the number of iteration reaches its limit. 
The time-step `dt` used by the code is chosen dynamically based on set tolerences the error in total energy and orbital angular momentum. If these 
errors are found to have a positive trend, the solver will not allow the time-step to be increased. If the errors are above the tolerences, the 
time-step will be systematically decreased every iteration until the errors are more reasonable. The code also implements a 'rollback' feature: this 
allows the simulation to re-run an iteration with a much smaller time-step in cases where running it with the previous time-step caused the total 
energy to jump discontinuously, and also in cases where the energy error is above a certain value. The code periodically saves the data representing 
the state of the system to a file, and outputs the current state of the system to stdout at set iteration intervals.


## The History File
This file stores the results of the most recent simulation.
By default, it is called 'history.dat' and is stored in './res/'. The first line in this file stores only an integer value, which is 
the number of bodies in the system. After the first line, the file is made up of sections - each representing a different iteration of the 
system. The first line of each section is the iteration number and the time. The following lines are each dedicated to a different body, giving 
its position and velocity in SI units and cartesian coordinates. This file is read by the the python script for plotting.

